#!/sbin/sh
# Written by Tkkg1994 for csc selection

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
	mount /dev/block/platform/155a0000.ufs/by-name/USERDATA /data
	mount /dev/block/platform/155a0000.ufs/by-name/EFS /efs
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
	mount /dev/block/platform/11120000.ufs/by-name/USERDATA /data
	mount /dev/block/platform/11120000.ufs/by-name/EFS /efs
fi

getprop ro.boot.bootloader >> /tmp/variant
ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
if [ ! -f /system/csc/sales_code.dat ]; then
	SALES_CODE=`cat /system/omc/sales_code.dat`
else
	SALES_CODE=`cat /system/csc/sales_code.dat`
fi

if grep -q  'CSC=AME\|CSC=CHE\|CSC=CPA\|CSC=CWW\|CSC=DBT\|CSC=DTM\|CSC=FTM\|CSC=O2U\|CSC=SKC\|CSC=UPO\|CSC=VD2\|CSC=XAA\|CSC=XAC\|CSC=XSA' /tmp/aroma/csc.prop; then
	echo "cscmulti.prop found"
	sed -i -- "s/CSC=//g" /tmp/aroma/cscmulti.prop
	NEW_CSC=`cat /tmp/aroma/cscmulti.prop`
else
	echo "csc.prop found"
	sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
	NEW_CSC=`cat /tmp/aroma/csc.prop`
fi

if [ ! -f /system/csc/sales_code.dat ]; then
	echo "OMC CSC found"
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
	echo "flashing selected CSC"
else
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/csc/sales_code.dat
	echo "flashing selected CSC"
fi

exit 10

